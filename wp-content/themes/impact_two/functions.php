<?php

/* 管理画面関連 -------------------------------*/

//ログイン画面のロゴを変更
// function login_logo(){
//     echo '<style type="text/css">
// .login h1 a {
// background-image:url('.get_template_directory_uri().'/admin-logo.png);
// background-size:274px,63px;
// width:274px;
// height:63px;
// }
// </style>';
// }
// add_action('login_head', 'login_logo');

// 使用しないメニューを非表示にする
// function remove_admin_menus() {
//     global $menu;
//     // unsetで非表示にするメニューを指定
//     unset($menu[2]);      // ダッシュボード
//     unset($menu[5]);      // 投稿
//     unset($menu[10]);     // メディア
//     unset($menu[20]);     // 固定ページ
//     unset($menu[25]);     // コメント
//     unset($menu[60]);     // 外観
//     unset($menu[65]);     // プラグイン
//     unset($menu[70]);     // ユーザー
//     unset($menu[75]);     // ツール
//     unset($menu[80]);     // 設定
// }
// add_action('admin_menu', 'remove_admin_menus');
//-----------------------------------------
// 投稿・固定ページ カスタムフィールド追加
//-----------------------------------------
add_action('admin_menu', 'add_custom_fields');
add_action('save_post', 'save_custom_fields');
function add_custom_fields() {
add_meta_box( 'my_sectionid', 'カスタムフィールド', 'my_custom_fields', 'post');
add_meta_box( 'my_sectionid', 'カスタムフィールド', 'my_custom_fields', 'page');
}

//-----------------------------------------
// カスタムフィールド表示
//-----------------------------------------
function my_custom_fields() {
global $post;
$robots = get_post_meta($post->ID,'robots',true);
$keywords = get_post_meta($post->ID,'keywords',true);
$description = get_post_meta($post->ID,'description',true);
echo '<p>検索ロボット（カンマ区切り）<br />';
echo '<input type="text" name="robots" value="'.esc_html($robots).'" size="50" /></p>';
echo '<p>キーワード（カンマ区切り）<br />';
echo '<input type="text" name="keywords" value="'.esc_html($keywords).'" size="50" /></p>';
echo '<p>ページの説明<br />';
echo '<input type="text" name="description" value="'.esc_html($description).'" size="50" /></p>';
}
//-----------------------------------------
// カスタムフィールドの値を保存
//-----------------------------------------
function save_custom_fields( $post_id ) {
if(!empty($_POST['robots']))
update_post_meta($post_id, 'robots', $_POST['robots'] );
else delete_post_meta($post_id, 'robots');
if(!empty($_POST['keywords']))
update_post_meta($post_id, 'keywords', $_POST['keywords'] );
else delete_post_meta($post_id, 'keywords');
if(!empty($_POST['description']))
update_post_meta($post_id, 'description', $_POST['description'] );
else delete_post_meta($post_id, 'description');
}
//-----------------------------------------
// ページ毎のmata要素とtitleの設定
//-----------------------------------------
function MataTitle() {
// カスタムフィールドの値を読み込む
$custom = get_post_custom();
if(!empty( $custom['robots'][0])) {
$robots = $custom['robots'][0];
}
if(!empty( $custom['keywords'][0])) {
$keywords = $custom['keywords'][0];
}
if(!empty( $custom['description'][0])) {
$description = $custom['description'][0];
}
?>
<?php if(is_home()): // トップページ ?>
<meta name="robots" content="index, follow" />
<meta name="keywords" content="キーワード1, キーワード2, キーワード3">
<meta name="description" content="サイトの説明文を入れて下さい。" />
<title><?php bloginfo('name'); ?></title>
<?php elseif(is_single()): // シングルページ ?>
<meta name="robots" content="index, follow" />
<meta name="keywords" content="<?php echo $keywords ?>">
<meta name="description" content="<?php echo $description ?>">
<title><?php wp_title('｜',true,'right'); bloginfo('name'); ?></title>
<?php elseif(is_page()): // 固定ページ ?>
<meta name="robots" content="<?php echo $robots ?>" />
<meta name="keywords" content="<?php echo $keywords ?>">
<meta name="description" content="<?php echo $description ?>">
<title><?php wp_title('｜',true,'right'); bloginfo('name'); ?></title>
<?php elseif (is_category()): // カテゴリーページ ?>
<meta name="robots" content="index, follow" />
<meta name="description" content="<?php single_cat_title(); ?>の記事一覧" />
<title><?php single_cat_title(); ?>の記事一覧 | <?php bloginfo('name'); ?></title>
<?php elseif (is_tag()): // タグページ ?>
<meta name="robots" content="noindex, follow" />
<meta name="description" content="<?php single_tag_title("", true); ?>の記事一覧" />
<title><?php single_tag_title(); ?>の記事一覧 | <?php bloginfo('name'); ?></title>
<?php elseif(is_404()): // 404ページ ?>
<meta name="robots" content="noindex, follow" />
<title><?php echo 'お探しのページが見つかりませんでした'; ?></title>
<?php else: // その他ページ ?>
<meta name="robots" content="noindex, follow" />
<title><?php wp_title('｜',true,'right'); bloginfo('name'); ?><?php bloginfo('name'); ?></title>
<?php endif; ?>
<?php
}


// エディタで自動整形制御 （htmlテキストエディタでpタグを自動生成させない）
add_action('init', function() {
    remove_filter('the_title', 'wptexturize');
    remove_filter('the_content', 'wptexturize');
    remove_filter('the_excerpt', 'wptexturize');
    remove_filter('the_title', 'wpautop');
    remove_filter('the_content', 'wpautop');
    remove_filter('the_excerpt', 'wpautop');
    remove_filter('the_editor_content', 'wp_richedit_pre');
});

add_filter('tiny_mce_before_init', function($init) {
    $init['wpautop'] = false;
    $init['apply_source_formatting'] = ture;
    return $init;
});



/* カスタム投稿・カスタム分類（タクソノミー）関連 ------------*/

// カスタム投稿登録
function register_cpt_custompost() {
//カスタム投稿の部分を任意の文字列へ変更
    $labels = array(
        'name'               => _x( 'イベント', 'event' ),
        'singular_name'      => _x( 'イベント', 'event' ),
        'add_new'            => _x( '新規追加', 'event' ),
        'add_new_item'       => _x( '新しいイベントを追加', 'event' ),
        'edit_item'          => _x( 'イベントを編集', 'event' ),
        'new_item'           => _x( 'イベントを追加', 'event' ),
        'view_item'          => _x( 'イベントを見る', 'event' ),
        'search_items'       => _x( 'イベント検索', 'event' ),
        'not_found'          => _x( 'イベントはありません', 'event' ),
        'not_found_in_trash' => _x( 'ゴミ箱にイベントはありません', 'event' ),
        'parent_item_colon'  => _x( '親 イベント:', 'event' ),
        'menu_name'          => _x( 'イベント', 'event' ),
    );

    $args = array(
        'labels'        => $labels,
        'public'        => true,
        'menu_position' => 5,                          // この投稿タイプが表示されるメニューの位置。5で投稿の下
        'menu_icon'     => 'dashicons-cart',  // 管理画面のメニューアイコン名 https://developer.wordpress.org/resource/dashicons/
        'supports'      => array(                      // このイベント内で使用する機能
            'title',                                     // （タイトル）
            'editor',                                    // （内容の編集）
            'author',                                    // （作成者）
            'thumbnail',                                 // （アイキャッチ画像）
            'excerpt',                                   // （抜粋）
            'trackbacks',                                // （トラックバック送信）
            'custom-fields',                             // （カスタムフィールド）
            'comments',                                  // （コメントの他、編集画面にコメント数のバルーンを表示する）
            'revisions',                                 // （リビジョンを保存する）
            'page-attributes',                           // （メニューの順序。「親〜」オプションを表示するために hierarchical が true であること）
            'post-formats'                               // （投稿のフォーマットを追加。投稿フォーマットを参照）
        ),
        'taxonomies'    => array( 'customcat' ),       // このイベントで使用するカスタム分類
        'has_archive'   => true                        // この投稿タイプのアーカイブを有効にする
    );

    register_post_type( 'event', $args );

    //カスタム分類（タクソノミー）登録
     register_taxonomy(
         'customcat',                 // カスタム分類の名前
         'event',                // このカスタム分類を使う投稿タイプ、もしくはカスタム投稿タイプ
         array(
             'label' => 'カスタム分類',   // カスタム分類表示名
             'hierarchical' => true   // trueでカテゴリーのように階層あり、falseでタグのように階層化なし
         )
     );
}
add_action( 'init', 'register_cpt_custompost' );



/* ヘッダ関連 ----------------------------------*/

// 不要ヘッダlink削除
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_shortlink_wp_head');



/* WordPressタグのカスタマイズ ---------------------*/

// 「続きを読む」をカスタマイズ
function my_excerpt_more($post) {
    return  '…<span><a href="'. get_permalink($post->ID) . '">' . '続きを見る' . '</a></span>';
}
function my_trim_all_excerpt( // 抜粋（the_excerpt()）を適当な文字数でカット
    $text = '' ,
    $cut  = 90 //表示する文字数
     ) {
    global $post;
    $raw_excerpt = $text;
    if ( '' == $text ) {
        $text = get_the_content('');
        $text = strip_shortcodes( $text );
        $text = apply_filters('the_content', $text);
        $text = str_replace(']]>', ']]>', $text);
        $text = strip_tags($text);
    }
    $excerpt_mblength = apply_filters('excerpt_mblength', $cut );
    $excerpt_more     = my_excerpt_more( $post );
    $text             = wp_trim_words( $text, $excerpt_mblength, $excerpt_more );
    return apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
}

remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'my_trim_all_excerpt' );



/* 投稿関連 -----------------------------------*/

function change_post_menu_label() {
global $menu;
global $submenu;
$menu[5][0] = 'News&blog';
$submenu['edit.php'][5][0] = 'News&blog一覧';
$submenu['edit.php'][10][0] = '新しいNews&blog';
$submenu['edit.php'][16][0] = 'タグ';
//echo ";
}
function change_post_object_label() {
global $wp_post_types;
$labels = &$wp_post_types['post']->labels;
$labels->name = 'NEWS';
$labels->singular_name = 'NEWS';
$labels->add_new = _x('追加', 'NEWS');
$labels->add_new_item = 'NEWSの新規追加';
$labels->edit_item = 'NEWSの編集';
$labels->new_item = '新規NEWS';
$labels->view_item = 'NEWSを表示';
$labels->search_items = 'NEWSを検索';
$labels->not_found = '記事が見つかりませんでした';
$labels->not_found_in_trash = 'ゴミ箱に記事は見つかりませんでした';
}
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );
// アイキャッチ画像を有効化
add_theme_support('post-thumbnails');

// メディア画像サイズ追加
// add_image_size( 'category', 148, 98, true );
// add_image_size( 'post', 300, 300, false );

// 特定のカテゴリーの投稿をショートコード[list num="表示数" cat="カテゴリーID"]で表示
function get_cat_items($atts, $content = null){
    extract(shortcode_atts(array(
        'num' => '5',  // デフォルトの表示数
        'cat' => '1'   // デフォルトのカテゴリーID
    ), $atts));
    global $post;
    $oldpost = $post;
    $myposts = get_posts('numberposts='.$num.'&category='.$cat);
    $retHtml = '';
    foreach($myposts as $post){
        setup_postdata($post);
        $retHtml.='<li><span>'.get_post_time('Y年m月d日').'</span><a href="'.get_the_permalink().'">'.mb_substr(get_the_title('','',false), 0, 40).'</a></li>'."\n";
    }
    $post = $oldpost;
    return $retHtml;
}
add_shortcode('list', 'get_cat_items');

// ショートコード[home_url]で表示
function get_code(){
    return home_url();
}
add_shortcode('home_url', 'get_code');

//ディレクトリパスをショートコードで[dir_url]
function allow_sc_template_dir($atts,$content='') {
    return get_template_directory_uri().$content;
}
add_shortcode('dir_url','allow_sc_template_dir');


//投稿の最初のイメージを表示させる
function catch_that_image() {
    global $post, $posts;
    $first_img = '';
    ob_start();
    ob_end_clean();
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
    $first_img = $matches [1] [0];
  
if(empty($first_img)){ //Defines a default image
        $first_img = "/img/default.jpg";
    }
    return $first_img;
}
//固定ページではビジュアルエディタを利用できないようにする
function disable_visual_editor_in_page(){
  global $typenow;
  if( $typenow == 'page' ){
    add_filter('user_can_richedit', 'disable_visual_editor_filter');
  }
}
function disable_visual_editor_filter(){
  return false;
}
add_action( 'load-post.php', 'disable_visual_editor_in_page' );
add_action( 'load-post-new.php', 'disable_visual_editor_in_page' );
?>