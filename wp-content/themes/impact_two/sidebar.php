        <div id="sub">
        	<p class="first_bnr"><img src="<?php echo get_template_directory_uri(); ?>/img/page/bnr_cafe.jpg" alt="英会話カフェ"></p>

            <div class="news">	
				<h2>最近の記事</h2>
				<ul class="post">
<?php query_posts( array(
     'post_type' => array('post', 'news'),
     'posts_per_page' => 5 ));
 ?>
<?php if (have_posts()) : while(have_posts()) : the_post(); ?>
    <li>
        <a href="<?php the_permalink() ?>">
            <?php the_time('Y/n/j') ?><br>
            <?php the_title(); ?></a></li>
<?php endwhile; endif; wp_reset_query(); ?>
</ul>
            </div>
            <div class="link">	
				<ul>
					<li><a href="<?php echo home_url(); ?>/careers"><img src="<?php echo get_template_directory_uri(); ?>/img/page/bnr_top03.jpg" alt="Careers"></a></li>
                    <!-- <li><a href="<?php echo home_url(); ?>/teacher"><img src="<?php echo get_template_directory_uri(); ?>/img/page/bnr_top02.jpg" alt="講師紹介"></a></li> -->
                    <li><a href="<?php echo home_url(); ?>/medical"><img src="<?php echo get_template_directory_uri(); ?>/img/page/bnr_top01.jpg" alt="ロサンゼルス治験に行こう"></a></li>
				</ul>
            </div>
            <div class="facebook">
			<div class="fb-page" data-href="https://www.facebook.com/impactlanguageschool/" data-tabs="timeline" data-width="290" data-height="600" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/impactlanguageschool/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/impactlanguageschool/">インパクト英会話</a></blockquote></div>
		</div>
        </div><!-- /#sub -->
