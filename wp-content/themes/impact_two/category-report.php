<?php
    $cat      = get_the_category();
    $cat      = $cat[0];
    $id   = $cat->term_id;
    $slug = $cat->slug;
    $name = $cat->name;
    if(isset($_GET['page'])){
        $paged = $_GET['page'];
    }
get_header(); ?>
    <div id="breadcrumb">
        <a href="<?php echo home_url(); ?>/">ホーム</a> &gt; <?php echo $name;?>
    </div>
    <div id="contents" class="<?php echo $slug;?>">
        <div id="main" class="category">
    <h1 id="page_title"><?php echo $cat->cat_name; ?></h1>
<?php $args = array(
    'cat'            => $id,
    'posts_per_page' => 5,
    'paged'          => $paged
); ?>
<?php $my_query = new WP_Query($args); ?>
<?php if ($my_query->have_posts()): while ($my_query->have_posts()) : $my_query->the_post(); ?>
            <div class="post">
                <p class="date"><?php the_time('Y.n.j') ?></p>
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <div class="cf">
                    <p class="pic"><img src="<?php echo catch_that_image(); ?>" alt="<?php the_title(); ?>"/></p>
                    <div class="excerpt">
                    <p class="text"><?php the_excerpt();?></p>
                </div>
                </div>
            </div><!-- /.post -->
<?php endwhile; ?>
            <div id="pager">
<?php
    global $wp_rewrite;
    $paginate_base = get_pagenum_link(1);
    if(strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()){
        $paginate_format = '';
        $paginate_base = add_query_arg('page','%#%');
    }
    else{
        $paginate_format = (substr($paginate_base,-1,1) == '/' ? '' : '/') . '?page=%#%';
        $paginate_base .= '%_%';
    }
    echo paginate_links(array(
        'base' => $paginate_base,
        'format' => $paginate_format,
        'total' => $my_query->max_num_pages,
        'mid_size' => 5,
        'current' => ($paged ? $paged : 1),
        'prev_text' => '＜',
        'next_text' => '＞',
        'add_args' => false
    ));
?>
            </div><!-- /#pager -->
<?php else : ?>
            <h3>記事はまだありません。</h3>
<?php
    endif;
    wp_reset_query();
?>
        </div><!-- /#main -->
<?php get_sidebar(); ?>
    </div><!-- /#contents -->
<?php get_footer(); ?>