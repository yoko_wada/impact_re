<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
  

 <?php MataTitle(); ?> 

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css">
<?php wp_head();?>
</head>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-96454349-1', 'auto');
  ga('send', 'pageview');

</script>
<body <?php body_class(); ?>>
<div id="fb-root"></div>
<!-- facebook -->
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<div id="wrapper">
    <div id="header">
        <div class="header_main cf">
            <div class="header_logo">
            <?php if(is_front_page()) :?>
                <h1><a href="<?php echo home_url(); ?>/"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png"　alt="大阪府豊中市の英会話スクール、蛍池ルシオーレ2階、Impact（インパクト）英会話"></a></h1>
            <?php else: ?> 
                 <p class="logo"><a href="<?php echo home_url(); ?>/"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png"　alt="大阪府豊中市の英会話スクール、蛍池ルシオーレ2階、Impact（インパクト）英会話"></a></p> 
            <?php endif; ?>  
            </div>
            <p class="bemore pc"><img src="<?php echo get_template_directory_uri(); ?>/img/head_txt.png" alt="Be more inspired empowered involved"></p>
            <div class="head_contact">
                <ul class="btn pc">
                    <li><a href="<?php echo home_url(); ?>/trial"><img src="<?php echo get_template_directory_uri(); ?>/img/contact_btn01.png" alt="無料体験レッスン"></a></li>
                    <li><a href="<?php echo home_url(); ?>/contact"><img src="<?php echo get_template_directory_uri(); ?>/img/contact_btn02.png" alt="お問い合わせ"></a></li>
                </ul>
            <ul class="tel pc">
                <li class="no"><img src="<?php echo get_template_directory_uri(); ?>/img/tel.png" alt="06-6398-7133"></li>
                <li class="time">10:00~20:00</li>   
            </ul>
            </div>
        </div>
        <ul class="head_link pc">
            <li><a href="<?php echo home_url(); ?>/news">NEWS</a></li>
            <li><a href="<?php echo home_url(); ?>/question">よくある質問</a></li>
            <li><a href="<?php echo home_url(); ?>/access">アクセス</a></li>
        </ul>
    </div><!-- /#header -->
        <div id="g_navi">
            <div id="h_menu" class="sp_menu sp">
                <div class="sp_menu_in"></div>
            </div>
            <ul class="nav_list cf">
                <li><a href="<?php echo home_url(); ?>/about">Impactの特色</a></li>
                <li><a href="<?php echo home_url(); ?>/course">コース内容</a></li>
                <li><a href="<?php echo home_url(); ?>/cost/">料金</a></li>
                <li><a href="<?php echo home_url(); ?>/studyabroad">Impactで留学</a></li>
                <li><a href="<?php echo home_url(); ?>/voice/">生徒さんの声</a></li>
                <li class="sp"><a href="<?php echo home_url(); ?>/news">NEWS</a></li>
                <li class="sp"><a href="<?php echo home_url(); ?>/question">よくある質問</a></li>
                <li class="sp"><a href="<?php echo home_url(); ?>/access">アクセス</a></li>
                <li><a href="<?php echo home_url(); ?>/contact/">お問い合わせ</a></li>
            </ul>
        </div><!-- /#g_navi -->
