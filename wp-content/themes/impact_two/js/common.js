$(function(){

// ページトップ フェードインアウト＆スムーススクロール
    var $scroll  = 100; // ページトップアイコン表示開始位置
    var $speed   = 300; // スクロール速度
    var $pagetop = $('#pagetop');

    // フェードインアウト
    $(window).on('scroll', function () {
        if($(this).scrollTop() > $scroll) {
            $pagetop.fadeIn();
            return false;
        }
        else {
            $pagetop.fadeOut();
            return false;
        }
    });

    // スムーススクロール
    $pagetop.on('click',function(){
        $('html,body').animate({
            scrollTop: 0
        }, $speed);
        return false;
    });

    // ページトップ要素が初期位置に来た時に固定(static,fixedクラスを操作)
    $(window).on('load', function () {
        var $footerOffset = $('#footer').offset().top;
        $(window).on('scroll', function () {
            if($(window).scrollTop() + $(window).height() < $footerOffset) {
                $pagetop.removeClass('static');
                $pagetop.addClass('fixed');
            } else {
                $pagetop.removeClass('fixed');
                $pagetop.addClass('static');
            }
        });
    });


    $("#h_menu").on("click", function() {
            $(this).next().slideToggle();
        });
    $(".faq_list").on("click", function() {
            $(this).next().slideToggle();
        });

$(function(){
      $('.height_box01').matchHeight();
       $('.height_box02').matchHeight();
        $('.height_box03').matchHeight();
         $('.height_box04').matchHeight();
          $('.height_box05').matchHeight();
           $('.height_box06').matchHeight();
    });


});