<?php get_header(); ?>
    <div id="breadcrumb">
        <a href="<?php echo home_url(); ?>/">ホーム</a> &gt; <?php the_title(); ?>
    </div>
    <div id="contents" class="<?php echo $slug;?>">
        <div id="main" class="page <?php echo $page->post_name;?>">
<?php $args = array(
    'cat'            => $id,
    'posts_per_page' => 5,
    'paged'          => $paged
); ?>
<?php $my_query = new WP_Query($args); ?>
<?php if ($my_query->have_posts()): while ($my_query->have_posts()) : $my_query->the_post(); ?>
    
    <div id="page_title">
        <h1><?php the_title();?></h1>
    </div>


<div class="post">
                <p class="date"><?php the_time('Y.n.j') ?></p>
                <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                <div class="cf">
                    <p class="pic"><img src="<?php echo catch_that_image(); ?>" alt="<?php the_title(); ?>"/></p>
                    <div class="excerpt">
                    <p class="text"><?php the_excerpt();?></p>
                </div>
                </div>
            </div><!-- /.post -->
<?php
    endwhile;
    endif;
    wp_reset_postdata();
?>
        </div><!-- /#main -->
<?php get_sidebar(); ?>
    </div><!-- /#contents -->
<?php get_footer(); ?>