<?php
    if(get_post_type() !== 'post'){
        $slug = get_post_type_object(get_post_type())->name;
        $name = get_post_type_object(get_post_type())->label;
    }else {
        $cat  = get_the_category();
        $cat  = $cat[0];
        $id   = $cat->term_id;
        $slug = $cat->slug;
        $name = $cat->name;
    }
get_header(); ?>
    <div id="breadcrumb">
        <a href="<?php echo home_url(); ?>/">ホーム</a> &gt; <a href="<?php echo home_url().'/'.$slug.'/'; ?>"><?php echo $name;?></a> &gt; <?php the_title(); ?>
    </div>
    <div id="contents" class="cf">
        <div id="main" class="single">
    <div id="page_title">
        <h1><?php the_title();?></h1>
    </div>
        <p class="date">投稿日：<?php the_time('Y/n/j') ?></p>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            
            <div class="article">
                <p><?php echo nl2br(get_the_content()); ?></p>
            </div><!-- /.article -->
            <ul id="page_navi">
<?php
$prev_post = get_previous_post();
$next_post = get_next_post();
if ( !empty( $prev_post ) ): ?>
                <li class="before"><?php previous_post_link('%link', '前へ', TRUE); ?></li>
<?php endif; ?>
                <li class="back"><a href="<?php echo home_url().'/'.$slug.'/'; ?>">一覧に戻る</a></li>
<?php
if ( !empty( $next_post ) ): ?>
                <li class="next"><?php next_post_link('%link', '次へ', TRUE); ?></li>
<?php endif; ?>
                
            </ul><!-- /#page_nav -->
<?php endwhile; ?>
<?php else : ?>
            <h3>記事はまだありません。</h3>
<?php
    endif;
    wp_reset_postdata();
?>
        </div><!-- /#main -->
<?php get_sidebar(); ?>
    </div><!-- /#contents -->
<?php get_footer(); ?>