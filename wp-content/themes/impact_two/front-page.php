<?php get_header(); ?>
    <div id="visual">
        <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/img/page/header_impact.jpg" alt="英会話で世界と対話しよう！豊中蛍池ルシオーレ インパクトコミュニケーション英会話"></p>
    </div><!-- /#visual -->
    <div id="contents">
        <div id="main" class="top">


        <div class="point area">
        	<h2 class="underline"><span>Impact英会話が選ばれる<br class="sp">４つのポイント</span></h2>
        	<ul class="point_list cf">
        		<li class="point01"><img src="<?php echo get_template_directory_uri(); ?>/img/page/top_img04.png" alt="point1、駅直結の便利な立地"></li>
        		<li class="point02"><img src="<?php echo get_template_directory_uri(); ?>/img/page/top_img05.png" alt="point2、レッスン費用が大手スクールの半分"></li>
        		<li class="point03"><img src="<?php echo get_template_directory_uri(); ?>/img/page/top_img06.png" alt="point3、日・祝日開校！好きな時間に予約できる"></li>
        		<li class="point04"><img src="<?php echo get_template_directory_uri(); ?>/img/page/top_img07.png" alt="point4、20年以上の経験豊富な先生がいる"></li>
        	</ul>
        	
        	
			<ul class="btn cf">
				<li class="link"><a href="<?php echo home_url(); ?>/about">Impactの特長へ</a></li>
				<li class="booking"><a href="<?php echo home_url(); ?>/trial">体験レッスンを予約する</a></li>
			</ul>
        </div>

	<div class="info cf">
	    <div id="lesson_flow" class="area">
			<h2 class="underline"><span>ご入会までの流れ</span></h2>
			<ol class="cf">
			 <li class="box height_box02">
			  <p class="step">Step<span>1</span></p>
			  <p class="inst">お問い合わせ</p>
			    <ul class="notes">
			      <li>お気軽に店頭へお越しください</li>
			      <li>電話　06-6398-7133</li>
			      <li><a href="<?php echo home_url(); ?>/contact">お問い合わせページへ</a></li>
			    </ul>
			  </li>
			  <li class="box height_box02">
			    <p class="step">Step<span>2</span></p>
			    <p class="inst">無料体験レッスン</p>
			    <p class="notes">カウンセリングで適した<br>コースをご提案し、無料レッスン<br>を体験していただけます。</p>
			  </li>
			  <li class="box height_box02">
			    <p class="step">Step<span>3</span></p>
			    <p class="inst"><br class="pc"><br class="pc">ご入会</p>
			  </li>
			  <li class="box height_box02">
			    <p class="step">Step<span>4</span></p>
			    <p class="inst">初レッスン</p>
			    <p class="notes">お好きな曜日・時間を選べます。<br />ご希望の方には無料でTOEIC/英検の<br>模擬試験を受けていただくことも可能です。</p>
			  </li>
			</ol>
			<div id="trial_detail" class="area cf">
				<h2 class="dots">無料体験レッスン受付中！</h2>
			<div class="txt_area">
				<p class="txt">無料体験レッスンでは、30分間レッスンが体験できます。</p>
				<p>インパクト英会話の無料体験レッスンは簡単な挨拶や自己紹介からスタートし、インストラクターと楽しくコミュニケーションを図りながら進めることができます。<br>体験レッスン終了後、インストラクター、カウンセラーよりお一人おひとりに合ったプランをお案内させていただきます。<br>
				お申し込みは無料体験レッスン申し込みフォームから、ぜひお気軽にお試しください。</p>
			</div>
			<p class="img"><img src="<?php echo get_template_directory_uri(); ?>/img/lessonroom.jpg" alt="体験レッスン教室写真"></p>
		</div>
			
		</div>
		
		<ul class="btn cf">
				<li class="link"><a href="<?php echo home_url(); ?>/cost">料金を見る</a></li>
				<li class="booking"><a href="<?php echo home_url(); ?>/trial">体験レッスンを予約する</a></li>
		</ul>
	<!-- 	<?php echo do_shortcode('[contact-form-7 id="1166" title="体験レッスンのお申込み・お問い合わせ"]'); ?> -->
	</div>

        <div class="notice area">
        	<h2 class="underline"><span>Seminar / Event</span></h2>
			<div class="block cf">
				  <?php $args = array(
				    'numberposts' => 3, //表示する記事の数
				    'post_type' => 'event' //投稿タイプ名
		 				   // 条件を追加する場合はここに追記
		 			 );
				  $customPosts = get_posts($args);
				  if($customPosts) : foreach($customPosts as $post) : setup_postdata( $post );
				  $cat = get_the_category(); 
				  $date = SCF::get( 'date' );
				  $time = SCF::get( 'time' );
				  ?>

			<div class="post">
				<a href="<?php the_permalink(); ?>">
				<div class="post_head">
					<p class="title height_box04"><?php echo mb_substr($post->post_title, 0, 40); ?></p>
				</div>
					<p class="pic"><?php the_post_thumbnail('full'); ?></p>
				</a>
					<p class="date height_box03">
						<?php echo esc_html( $date ); ?>
						<span><?php echo esc_html( $time ); ?></span>
					</p>
					<p class="entry height_box05">	
<?php print mb_substr(get_the_excerpt(), 0, 83); ?>...</p>
		<p class="to_page"><a href="<?php the_permalink(); ?>">詳細ページへ</a></p>

			</div>
				  <?php endforeach; ?>
				  <?php else : //記事が無い場合 ?>
				  	<p>ただ今実施予定のイベントはありません。</p>
				  
				  <?php endif;
				  wp_reset_postdata(); //クエリのリセット ?>
				
			
			</div>
			<p class="more"><a href="<?php echo home_url(); ?>/event">もっと見る</a></p>
			<p class="btn"><a href="<?php echo home_url(); ?>/contact">セミナー・イベントに参加する</a></p>
			<p class="report"><a href="<?php echo home_url(); ?>/report"><img src="<?php echo get_template_directory_uri(); ?>/img/page/btn_report.png" alt="イベントレポートはこちら"></a></p>


        </div><!-- .notice -->

	<div id="news" class="area cf">
	<h2 class="underline"><span>News / Blog</span></h2>
	<?php while (have_posts()) : the_post(); ?>
<?php the_content(); ?>
<?php endwhile; ?>
		<div class="timeline">
				<?php
				   $newslist = get_posts( array(
				    'category_name' => 'news', //特定のカテゴリースラッグを指定
   					'posts_per_page' => 4 //取得記事件数
				   ));
				   foreach( $newslist as $post ):
				   setup_postdata( $post );
				?>
				<div class="block cf">
				<a href="<?php the_permalink(); ?>">
					<p class="pic"><img src="<?php echo catch_that_image(); ?>" alt="<?php the_title(); ?>"/></p>
					<div class="txt cf">
						<h3 class="title"><?php
								$title= $post->post_title;
								echo $title;
								?>	
						</h3> 
						<p class="date"><?php the_time('Y/n/j') ?></p>
						<p class="txt pc"><?php the_excerpt();?></p>
					</div>
				</a>
				</div>

			<?php
					  endforeach;
					  wp_reset_postdata();
					?>

				<p class="more"><a href="<?php echo home_url(); ?>/news">もっと見る</a></p>

		</div>

		<div class="facebook">
			<div class="fb-page" data-href="https://www.facebook.com/impactlanguageschool/" data-tabs="timeline" data-width="290" data-height="600" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/impactlanguageschool/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/impactlanguageschool/">インパクト英会話</a></blockquote></div>
		</div>	

	</div>
	<div class="greeting">
    	<h3 class="dots">ごあいさつ</h3>
    	<div class="txt_area cf">
    		<p class="txt">小規模スクールだからできる、きめ細やかなサービスを</p>
    		<p class="img"><img src="<?php echo get_template_directory_uri(); ?>/img/img_greeting.png" alt="代表：宮原味佐子"></p>
    		<p class="txt02">色々なバックグラウンドの方が来られていて、ほとんどの生徒さんが目標を達成されています。<br>小規模なスクールですので、あらゆることにフレキシブルに対応できます。学習内容の変更や、突然のスケジュール変更への対応など、大手英会話スクールにはできないきめ細やかなサービスを心がけております。<br>私たちの講師陣はハートが違います。また、それぞれ専門性を持ち、一芸に秀でております。<br>努力は裏切りません。始めていただければ必ずゴールまでご案内します。さあ、一緒にスタートしましょう。</p>
    	</div>
	</div>
        <div class="footlink">
        	<ul class="cf">
	            <li><a href="<?php echo home_url(); ?>/careers"><img src="<?php echo get_template_directory_uri(); ?>/img/page/bnr_top03.jpg" alt="Careers"></a></li>
	           <!--  <li><a href="<?php echo home_url(); ?>/teacher"><img src="<?php echo get_template_directory_uri(); ?>/img/page/bnr_top02.jpg" alt="講師紹介"></a></li> -->
	            <li><a href="<?php echo home_url(); ?>/medical"><img src="<?php echo get_template_directory_uri(); ?>/img/page/bnr_top01.jpg" alt="ロサンゼルス治験に行こう"></a></li>
	        </ul>
    	</div>

        </div><!-- /#main -->
    </div><!-- /#contents -->
<?php get_footer(); ?>