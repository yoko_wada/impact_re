
    <div id="footer">
        <p id="pagetop"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/pagetop.png"　alt="topへ戻る"></a></p>
        <div class="footer_main cf">
            <p class="logo"><a href="<?php echo home_url(); ?>/"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png"　alt="豊中蛍池ルシオーレImpactコミュニケーション英会話"></a></p>
            <ul class="address">
                <li><img src="<?php echo get_template_directory_uri(); ?>/img/tel.png" alt="06-6398-7133"></li>
                <li>豊中市蛍池中町３−２−１　ルシオーレ２階</li>
            </ul>
            <dl class="sns cf">
                <dt>Follow me!</dt>
                <dd><a href="https://www.facebook.com/impactlanguageschool/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png" alt="facebook"></a></dd>
                <dd><a href="https://www.instagram.com/impacteikaiwa/?hl=ja" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/insta.png" alt="instagram"></a></dd>
            </dl>
        </div><!-- /#footer_main -->
            <div id="footer_contact">
                
                <ul class="contact cf">
                    <li><a href="<?php echo home_url(); ?>/trial"><img src="<?php echo get_template_directory_uri(); ?>/img/contact_btn01.png" alt="無料体験レッスン"></a></li>
                    <li><a href="<?php echo home_url(); ?>/contact"><img src="<?php echo get_template_directory_uri(); ?>/img/contact_btn02.png" alt="お問い合わせ"></a></li>
                </ul>
            </div>
            
            <ul id="f_navi" class="cf">
                <li><a href="<?php echo home_url(); ?>/">ホーム</a></li>
                <li><a href="<?php echo home_url(); ?>/news">NEWS</a></li>
                <li><a href="<?php echo home_url(); ?>/about">Impactの特長</a></li>
                <li><a href="<?php echo home_url(); ?>/course">コース内容</a></li>
                <li><a href="<?php echo home_url(); ?>/cost">料金</a></li>
                <li><a href="<?php echo home_url(); ?>/question">よくある質問</a></li>
                <li><a href="<?php echo home_url(); ?>/access">アクセス</a></li>
                <li><a href="<?php echo home_url(); ?>/contact/">お問い合わせ</a></li>

                <!-- <li><a href="<?php echo home_url(); ?>/policy/">プライバシーポリシー</a></li>
                <li><a href="<?php echo home_url(); ?>/sitemap/">サイトマップ</a></li> -->
            </ul><!-- /#f_navi -->
        <p class="copy">&copy; 2017&nbsp;Impact英会話 All Rights Reserved.</p>
    </div><!-- /#footer -->
</div><!-- /#wrapper -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.11.2.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/common.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.matchHeight.js"></script>
<?php wp_footer(); ?>
</body>
</html>