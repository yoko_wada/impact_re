<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php $page = get_page(get_the_ID()); ?>
    <div id="breadcrumb">
        <a href="<?php echo home_url(); ?>/">ホーム</a> &#9655; <?php the_title(); ?>
    </div>
    <div id="contents" class="cf">
        <div id="main" class="page <?php echo $page->post_name;?>">
    <div id="page_title">
        <h1><?php the_title();?></h1>
    </div>



<article class="article">


<?php the_content(); ?>
</article>
<?php
    endwhile;
    endif;
    wp_reset_postdata();
?>
        </div><!-- /#main -->
<?php get_sidebar(); ?>
    </div><!-- /#contents -->
<?php get_footer(); ?>