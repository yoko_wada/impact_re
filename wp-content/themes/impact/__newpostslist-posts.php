<?php
        $show_count = 5;
        $loop = new WP_Query( array( 'post_type' => 'post' , 'posts_per_page' => $show_count) ); 
		if ($loop->post_count>0) :
	            echo "<div class=\"newpostlists\">";
	            echo "<dl>";
	            while ( $loop->have_posts() ) : $loop->the_post();
	                get_template_part( 'loop', 'side' );
	            endwhile;
	            echo "</dl>";
	            if ($loop->post_count > $show_count){
	                $page = get_page_by_path('blog');
	                $permalink = get_permalink( $page->ID, false );
	                echo "<p class=\"more\"><a href=\"$permalink\">過去のお知らせを見る</a>";
	            }
	            echo "</div>";
		endif;
        wp_reset_postdata();