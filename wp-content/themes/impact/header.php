<!DOCTYPE html><!-- HTML 5 -->
<html <?php language_attributes(); ?>>

<head>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	
	<title><?php wp_title('|', true, 'right'); ?></title>

<?php wp_head(); ?>
</head>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-96454349-1', 'auto');
  ga('send', 'pageview');

</script>
<body <?php body_class(); ?>>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- <?php themezee_wrapper_before(); // hook before #wrapper ?> -->
<div id="wrapper">

<!-- 	<?php themezee_header_before(); // hook before #header ?> -->
	<div id="header">

		<div id="head">
			<div id="logo">
				<?php 
				$options = get_option('themezee_options');
				if ( isset($options['themeZee_general_logo']) and $options['themeZee_general_logo'] <> "" ) { ?>
					<a href="<?php echo home_url(); ?>"><?php theme_logo(); ?></a>
				<?php } else { ?>
					<a href="<?php echo home_url(); ?>/"><h1><?php bloginfo('name'); ?></a>
				<?php } ?>
			</div>
			<div id="logo_narrow">
				<?php 
				$options = get_option('themezee_options');
				 
                                    $logoPathInfo = pathinfo (get_option("tl_logo_src"));
                                    $narrowlogoUrl = $logoPathInfo['dirname']."/".$logoPathInfo['filename']."_narrow.".$logoPathInfo['extension'];
                                     ?>
                            <a href="<?php echo home_url(); ?>"><img src="<?php echo $narrowlogoUrl ?>" alt="<?php echo get_option("blogname")?>"></a>
			</div>
                            <?php 
ob_start();
                                // Get Navigation out of Theme Options
                                wp_nav_menu(array('theme_location' => 'top_navi', 'container' => false, 'menu_id' => 'topnav', 'echo' => true, 'fallback_cb' => '', 'before' => '', 'after' => '', 'link_before' => '', 'link_after' => '', 'depth' => 0));
$topNav = ob_get_contents();
ob_end_clean();
if ($topNav){echo '<div id="topnavi">'.$topNav.'</div>'; }
                            ?>
			
		</div>
	</div>
	<?php themezee_header_after(); // hook after #header ?>
    <div id="wrap_outer">
	<div id="wrap">
		<div id="navi">
			<?php 
				// Get Navigation out of Theme Options
				wp_nav_menu(array('theme_location' => 'main_navi', 'container' => false, 'menu_id' => 'nav', 'echo' => true, 'fallback_cb' => 'themezee_default_menu', 'before' => '', 'after' => '', 'link_before' => '', 'link_after' => '', 'depth' => 0));
			?>
		</div>
		<div class="clear"></div>
		<div id="navi_narrow_bar">
                    <div class="sb-toggle-left navbar-left">
                        <div class="navicon-line"></div>
                        <div class="navicon-line"></div>
                        <div class="navicon-line"></div>
                    </div>
                    <div class="navi_narrow_bar_caption">
                        メニュー
                        </div>
		</div>
		<div id="navi_narrow">
			<?php 
				// Get Navigation out of Theme Options
				wp_nav_menu(array('theme_location' => 'main_navi', 'container' => false, 'menu_id' => 'nav_narrow', 'echo' => true, 'fallback_cb' => 'themezee_default_menu', 'before' => '', 'after' => '', 'link_before' => '', 'link_after' => '', 'depth' => 0));
			?>
		</div>
		<div class="clear"></div>
                
		<?php 
                $headerPathInfo = pathinfo (get_header_image());
                $narrowheaderUrl = $headerPathInfo['dirname']."/".$headerPathInfo['filename']."_narrow.".$headerPathInfo['extension'];
                 ?>
	<?php if( get_header_image() != '' and !is_page_template('template-frontpage.php') ) : ?>
		<div id="custom_header">
			<img src="<?php echo get_header_image(); ?>" />
		</div>
		<div id="custom_header_narrow">
			<img src="<?php echo $narrowheaderUrl; ?>" />
		</div>
	<?php endif; ?>