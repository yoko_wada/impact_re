<?php


// スタイルシートとスクリプトの読み込みコード
function responsive_impact_scripts() {
    wp_enqueue_style( 'responsive-impact-style', get_stylesheet_directory_uri()."/responsive_style.css", array( 'genericons' ) );
    wp_enqueue_script( 'responsive-impact-jquery', get_stylesheet_directory_uri() . '/responsive.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'responsive_impact_scripts' );
