jQuery(function($) {
    var wrapper_width;
    var menu_open = 0;
    $('#navi_narrow_bar' ).on( 'click', function(event){
        if (menu_open==0){
            event.stopPropagation();
            wrapper_width = $('#wrapper').width();
            var wrapper_position = $('#navi_narrow_bar').position();
            $('#wrapper').width(wrapper_width);
            $('#wrapper').animate({'left':'220px'},500);
            $('#navi_narrow').css('top',wrapper_position.top);
            $('#navi_narrow').animate({'right':'0'},500,function(){menu_open = 1;});
            
            return false;
        }
    }); 
    $('#wrapper' ).on( 'click', function(){
        if (menu_open==1){
            $('#wrapper').width(wrapper_width);
            $('#wrapper').animate({'left':'0'},500);
            $('#navi_narrow').animate({'right':'-100%'},500,function(){
                $('#wrapper').removeAttr('style')
            });
            //$('#wrapper').removeAttr('style')
            menu_open = 0;
        }
    }); 
});